import 'dart:convert';
import 'package:bookexchange/Models/User.dart';

class LoginResponse {
  bool ok = false;
  String msg;
  User usuario;

  LoginResponse(
    this.ok,
    this.msg,
    this.usuario,
  );

  LoginResponse copyWith({bool ok, String msg, User usuario}) {
    return LoginResponse(
      ok ?? this.ok,
      msg ?? this.msg,
      usuario ?? this.usuario,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'ok': ok,
      'msg': msg,
      'usuario': usuario?.toMap(),
    };
  }

  factory LoginResponse.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    return LoginResponse(
      map['ok'],
      map['msg'] ?? "",
      User.fromMap(map['usuario'] ?? null),
    );
  }

  String toJson() => json.encode(toMap());

  factory LoginResponse.fromJson(String source) =>
      LoginResponse.fromMap(json.decode(source));

  @override
  String toString() => 'LoginResponse(ok: $ok, mesg: $msg, usuario: $usuario)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is LoginResponse &&
        o.ok == ok &&
        o.msg == msg &&
        o.usuario == usuario;
  }

  @override
  int get hashCode => ok.hashCode ^ msg.hashCode ^ usuario.hashCode;
}
