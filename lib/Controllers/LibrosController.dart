import 'dart:convert';

import 'package:bookexchange/Helpers/Preferences.dart';
import 'package:bookexchange/Models/Libro.dart';
import 'package:bookexchange/Resources/Variables.dart';
import 'package:http/http.dart' as http;

class LibrosController {
  /// Envia solicitud al BackEnd para crear nuevo Libro
  ///
  ///  @param [libro] `Libro`
  ///
  /// @return `Map`
  static Future<Map<String, dynamic>> crearLibro(Libro libro) async {
    final uri = url("/books/create");
    final token = await Preferences.getToken();
    try {
      final response = await http.post(uri,
          headers: {"Content-Type": "application/json", "token": token},
          body: libro.toJson());
      return jsonDecode(response.body);
    } catch (e) {
      return {"ok": false, "msg": e.toString()};
    }
  }

  static Future<Map<String, dynamic>> getLibro(String codigoBarras) async {
    try {
      final uri = url("/books/$codigoBarras");
      final response = await http.get(uri);
      return jsonDecode(response.body);
    } catch (e) {
      return {"res": false, "msg": e.toString()};
    }
  }

  static Future<Map<String, dynamic>> checkLibro(String codigoBarras) async {
    try {
      final token = await Preferences.getToken();
      final uri = url("/books/checkLibro/$codigoBarras");
      final response = await http.get(uri, headers: {"token": token});
      return jsonDecode(response.body);
    } catch (e) {
      return {"res": false, "msg": e.toString()};
    }
  }
}
