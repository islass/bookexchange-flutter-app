import 'dart:convert';

import 'package:bookexchange/Helpers/Preferences.dart';
import 'package:bookexchange/Models/Publicacion.dart';
import 'package:bookexchange/Resources/Variables.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;

class PublicacionesController {
  static Future<Map<String, dynamic>> getPublicaciones(
      String geohash, double latitude, double longitude) async {
    final uri = url("/publicaciones/position/$geohash/$latitude/$longitude");
    final response = await http.get(uri);
    return jsonDecode(response.body);
  }

  static Future createPublicacion(String idLibro, Position position) async {
    try {
      final posicion = CustomPosition(coordenadas: position);
      final json =
          jsonEncode({"libro": idLibro, "posicion": posicion.toJson()});
      final token = await Preferences.getToken();
      final uri = url("/publicaciones/create");

      final response = await http.post(uri,
          headers: {
            "token": token,
            "Content-Type": "application/json",
          },
          body: json);
      return jsonDecode(response.body);
    } catch (e) {
      return {"ok": false, "msg": e.toString()};
    }
  }
}
