import 'package:bookexchange/Resources/Variables.dart' as variables;
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:bookexchange/Models/User.dart';

class UserController {
  static Future<http.Response> register(User usuario) async {
    final uri = variables.url("/user/register");
    // var url = Uri.http("192.168.8.111:3000", "/api/user/register");
    final response = await http.post(
      uri,
      headers: {"Content-Type": "application/json"},
      body: usuario.toJson(),
    );
    print(response.body);
    return response;
  }

  static Future logIn(User usuario) async {
    final uri = variables.url("/user/login");
    try {
      final response = await http.post(uri,
          headers: {"Content-Type": "application/json"},
          body: usuario.toJson());
      return jsonDecode(response.body);
    } catch (e) {
      return {"ok": false, "msg": e.toString()};
    }
  }

  static Future<Map<String, dynamic>> refreshToken(String token) async {
    try {
      final url = variables.url("/user/refreshToken");
      final response = await http.get(url, headers: {"token": token});
      final res = jsonDecode(response.body);
      return res;
    } catch (e) {
      return {"ok": null, "msg": e.toString()};
    }
  }
}
