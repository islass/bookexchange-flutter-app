import 'dart:convert';

import 'package:bookexchange/Models/Libro.dart';
import 'package:flutter/material.dart';


class User {
  List<Libro> libros;
  String name;
  String email;
  String password;
  bool isPublic;
  String idUsuario;

  User.empty();

  User({
    this.libros,
    @required this.name,
    @required this.email,
    @required this.isPublic,
    @required this.idUsuario,
  });

  User.private({
    @required this.name,
    @required this.email,
    @required this.isPublic,
    @required this.idUsuario,
  });

  factory User.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    if (map["isPublic"]) {
      return User(
        libros: (map["libros"] != null)
            ? (map["libros"] as Iterable).map((x) => Libro.fromMap(x)).toList()
            : null,
        name: map["name"],
        email: map["email"],
        isPublic: map["isPublic"],
        idUsuario: map["idUsuario"],
      );
    } else {
      return User.private(
        name: map["name"],
        email: map["email"],
        isPublic: map["isPublic"],
        idUsuario: map["id"],
      );
    }
  }

  Map<String, dynamic> toMap() => {
        "libros": libros,
        "name": name,
        "email": email,
        "isPublic": isPublic,
        "id": idUsuario,
        "password": password ?? ""
      };

  String toJson() {
    return json.encode(this.toMap());
  }
}
