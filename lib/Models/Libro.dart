import 'dart:convert';

class Libro {
  int noPaginas;
  List<String> fotos;
  String codigoBarras;
  String titulo;
  String autor;
  String descripcion;
  String propietario;
  String idLibro;

  Libro({
    this.noPaginas = 0,
    this.fotos,
    this.codigoBarras = "",
    this.titulo = "",
    this.autor = "",
    this.descripcion = "",
    this.propietario = "",
    this.idLibro = "",
  });

  factory Libro.fromMap(Map<String, dynamic> json) => Libro(
        noPaginas: json["no_Paginas"],
        fotos: List<String>.from(json["fotos"].map((x) => x)),
        codigoBarras: json["codigo_barras"],
        titulo: json["titulo"],
        autor: json["autor"],
        descripcion: json["descripcion"],
        propietario: json["propietario"],
        idLibro: json["idLibro"],
      );

  Map<String, dynamic> toMap() => {
        "no_Paginas": noPaginas,
        "fotos": (fotos != null) ? List<String>.from(fotos.map((x) => x)) : "",
        "codigo_barras": codigoBarras,
        "titulo": titulo,
        "autor": autor,
        "descripcion": descripcion,
        "propietario": propietario,
        "idLibro": idLibro,
      };

  Libro copyWith({
    int noPaginas,
    List<String> fotos,
    String codigoBarras,
    String titulo,
    String autor,
    String descripcion,
    String propietario,
    String idLibro,
  }) {
    return Libro(
      noPaginas: noPaginas ?? this.noPaginas,
      fotos: fotos ?? this.fotos,
      codigoBarras: codigoBarras ?? this.codigoBarras,
      titulo: titulo ?? this.titulo,
      autor: autor ?? this.autor,
      descripcion: descripcion ?? this.descripcion,
      propietario: propietario ?? this.propietario,
      idLibro: idLibro ?? this.idLibro,
    );
  }

  String toJson() => json.encode(toMap());

  factory Libro.fromJson(String source) => Libro.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Libro(noPaginas: $noPaginas, fotos: $fotos, codigoBarras: $codigoBarras, titulo: $titulo, autor: $autor, descripcion: $descripcion, propietario: $propietario, idLibro: $idLibro)';
  }
}
