import 'package:flutter/material.dart';
import 'package:geohash/geohash.dart';
import 'package:geolocator/geolocator.dart';

import 'package:bookexchange/Models/Libro.dart';
import 'package:bookexchange/Models/User.dart';

class Publicacion {
  CustomPosition posicion;
  String status;
  String fecha;
  List<String> preferenciasLibros;
  User usuario;
  Libro libro;
  String idPublicacion;
  String distancia;

  Publicacion({
    this.posicion,
    this.status,
    this.fecha,
    this.preferenciasLibros,
    this.usuario,
    this.libro,
    this.idPublicacion,
    this.distancia,
  });

  factory Publicacion.fromMap(Map<String, dynamic> map) => Publicacion(
        posicion: CustomPosition.fromMap(map["posicion"]),
        status: map["status"],
        fecha: map["fecha"],
        preferenciasLibros:
            List<String>.from(map["preferenciasLibros"].map((x) => x)),
        usuario: User.fromMap(map["usuario"]),
        libro: Libro.fromMap(map["libro"]),
        idPublicacion: map["idPublicacion"],
        distancia: map["distancia"],
      );

  Map<String, dynamic> toMap() => {
        "posicion": posicion.toJson(),
        "status": status,
        "fecha": fecha,
        "preferenciasLibros":
            List<dynamic>.from(preferenciasLibros.map((x) => x)),
        "usuario": usuario.toJson(),
        "libro": libro.toJson(),
        "idPublicacion": idPublicacion,
        "distancia": distancia,
      };
}

class CustomPosition {
  String geohash = "";
  Position coordenadas;

  CustomPosition({@required this.coordenadas, this.geohash}) {
    if (this.geohash == null) {
      this.geohash =
          Geohash.encode(this.coordenadas.latitude, this.coordenadas.longitude);
    }
  }

  factory CustomPosition.fromMap(Map<String, dynamic> map) => CustomPosition(
      geohash: map["geohash"],
      coordenadas: Position(
          latitude: map["coordenadas"]["lat"],
          longitude: map["coordenadas"]["long"]));

  Map<String, dynamic> toJson() => {
        "coordenadas": {
          "lat": coordenadas.latitude,
          "long":coordenadas.longitude
        },
        "geohash": geohash,
      };
}
