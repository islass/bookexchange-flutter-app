import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Uri url(String endPoint) {
  return Uri.http('192.168.8.109:3000', "/api" + endPoint);
}

Future goTo(BuildContext context, Widget ruta) {
  return Navigator.push(
    context,
    MaterialPageRoute(
      builder: (context) => ruta,
    ),
  );
}

Future replaceTo(BuildContext context, Widget ruta) {
  return Navigator.pushReplacement(
    context,
    MaterialPageRoute(
      builder: (context) => ruta,
    ),
  );
}

