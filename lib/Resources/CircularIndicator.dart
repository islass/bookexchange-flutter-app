import 'package:flutter/material.dart';

class CircularIndicator extends StatelessWidget {
  final String texto;
  const CircularIndicator({
    this.texto = "",
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CircularProgressIndicator(),
            SizedBox(
              height: 20,
            ),
            Text(this.texto),
          ],
        ),
      ),
    );
  }
}
