import 'package:bookexchange/main.dart';
import 'package:flutter/material.dart';

class ConexionState extends StatelessWidget {
  const ConexionState({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.warning,
              size: 40,
              color: Colors.deepOrangeAccent,
            ),
            Text("No hay conexón a internet"),
            RaisedButton(
              onPressed: () {
                Navigator.pushReplacement(
                    context, MaterialPageRoute(builder: (_) => MyApp()));
              },
              child: Text("Reintentar"),
            )
          ],
        ),
      ),
    );
  }
}
