import 'package:flutter/material.dart';

Future buildShowDialog(BuildContext context, String titulo, String response) {
  return showDialog(
    context: context,
    builder: (context) => AlertDialog(
      title: Text(titulo),
      content: Text(response),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text("Aceptar"),
        ),
      ],
    ),
  );
}
