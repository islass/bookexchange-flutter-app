import 'dart:io';

import 'package:bookexchange/Controllers/LibrosController.dart';
import 'package:bookexchange/Controllers/PublicacionesController.dart';
import 'package:bookexchange/Models/Libro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker_gallery_camera/image_picker_gallery_camera.dart';

enum Status { registrando, none }

class LibroProvider extends ChangeNotifier {
  List<File> fotos;
  Libro libro;
  TextEditingController barCodeController;
  TextEditingController tituloController;
  TextEditingController autorController;
  TextEditingController descripcionController;
  TextEditingController noPaginasController;
  FocusNode tituloFocusNode;
  FocusNode autorFocusNode;
  FocusNode descripcionFocusNode;
  FocusNode noPaginasFocusNode;
  GlobalKey<FormState> formKey;

  Status status = Status.none;

  set barCode(String barcode) {
    this.libro.codigoBarras = "";
    barCodeController.text = "";
    notifyListeners();
  }

  set titulo(String titulo) {
    this.libro.titulo = titulo.toUpperCase();
    notifyListeners();
  }

  set autor(String autor) {
    this.libro.autor = autor.toUpperCase();
    notifyListeners();
  }

  set descripcion(String descripcion) {
    this.libro.descripcion = descripcion.toUpperCase();
    notifyListeners();
  }

  set noPaginas(int noPaginas) {
    this.libro.noPaginas = noPaginas ?? 0;
    this.notifyListeners();
  }

  bool get check {
    if (this.libro.autor != null &&
        this.libro.codigoBarras != null &&
        this.libro.titulo != null &&
        this.libro.descripcion != null &&
        (this.libro.noPaginas > 0 && this.libro.noPaginas != null)) {
      if (this.libro.autor.isNotEmpty &&
          this.libro.titulo.isNotEmpty &&
          this.libro.descripcion.isNotEmpty &&
          this.libro.noPaginas > 0) {
        return true;
      }
      return true;
    } else {
      return false;
    }
  }

  LibroProvider() {
    this.fotos = List<File>();
    this.libro = Libro();
    this.formKey = GlobalKey<FormState>();
    this.tituloController = TextEditingController(text: this.libro.titulo);
    this.autorController = TextEditingController(text: this.libro.autor);
    this.descripcionController =
        TextEditingController(text: this.libro.descripcion);
    this.noPaginasController =
        TextEditingController(text: this.libro.noPaginas.toString());
    this.barCodeController = TextEditingController();
    tituloFocusNode = FocusNode();
    autorFocusNode = FocusNode();
    descripcionFocusNode = FocusNode();
    noPaginasFocusNode = FocusNode();
  }

  Future<void> addFotos(BuildContext context) async {
    try {
      final picker = await ImagePickerGC.pickImage(
          context: context, source: ImgSource.Both);
      if (picker != null) this.fotos.add(picker);
      notifyListeners();
    } on Exception catch (e) {
      print(e.toString());
    }
  }

  List<Widget> getFotos(BuildContext context) {
    return this
        .fotos
        .map((foto) => Row(
              children: <Widget>[
                Stack(
                  alignment: Alignment.topRight,
                  children: <Widget>[
                    Card(
                      shadowColor: Colors.black,
                      elevation: 5,
                      child: Image.file(
                        foto,
                        fit: BoxFit.fill,
                        // width: MediaQuery.of(context).size.width * .5,
                        height: MediaQuery.of(context).size.height,
                      ),
                    ),
                    IconButton(
                      tooltip: "Eliminar foto",
                      // color: Colors.red,
                      onPressed: () {
                        this.fotos.remove(foto);
                        notifyListeners();
                      },
                      icon: Container(
                        // alignment: Alignment.topLeft,
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          boxShadow: [BoxShadow(offset: Offset(2, 3))],
                          borderRadius: BorderRadius.all(
                            Radius.circular(10),
                          ),
                        ),
                        child: Icon(
                          Icons.close,
                          color: Colors.redAccent[400],
                        ),
                      ),
                    )
                  ],
                ),
                VerticalDivider()
              ],
            ))
        .toList();
  }

  Future scanBarCode(String propietario) async {
    try {
      final codeBar = await FlutterBarcodeScanner.scanBarcode(
          "red", "Cancelar", false, ScanMode.BARCODE);
      if (codeBar != "-1") {
        this.barCodeController.text = codeBar;
        notifyListeners();
        this.libro.codigoBarras = codeBar != "-1" ? codeBar : null;
        final response = await LibrosController.checkLibro(codeBar);
        if (response["ok"] == true) {
          if (response["msg"] != null) {
            return response;
          }
          final libroTemp = Libro.fromMap(response["libro"]);
          this.tituloController.text = libroTemp.titulo;
          this.autorController.text = libroTemp.autor;
          this.descripcionController.text = libroTemp.descripcion;
          this.noPaginasController.text = libroTemp.noPaginas.toString();
          this.libro.titulo = libroTemp.titulo;
          this.libro.autor = libroTemp.autor;
          this.libro.descripcion = libroTemp.descripcion;
          this.libro.noPaginas = libroTemp.noPaginas;

          notifyListeners();
          // this.libro.fotos
        } else {
          this.libro.propietario = propietario;
          notifyListeners();
        }
      }
      notifyListeners();
    } on Exception catch (e) {
      return {"ok": false, "msg": e.toString()};
    }
  }

  Future crearLibro(Position position) async {
    this.status = Status.registrando;
    notifyListeners();
    final response = await LibrosController.crearLibro(this.libro);
    if (response["ok"]) {
      final libro = Libro.fromMap(response["libro"]);
      final response2 = await PublicacionesController.createPublicacion(
        libro.idLibro,
        position,
      );
      this.status = Status.none;
      notifyListeners();
      return response2;
    } else {
      this.status = Status.none;
      notifyListeners();
      return response;
    }
  }

  void limpiarCampos() {
    this.barCodeController.text = "";
    this.tituloController.text = "";
    this.autorController.text = "";
    this.descripcionController.text = "";
    this.noPaginasController.text = "";
    notifyListeners();
  }
}
