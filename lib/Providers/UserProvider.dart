import 'package:bookexchange/Helpers/Preferences.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:bookexchange/Controllers/UserController.dart';
import 'package:bookexchange/Models/User.dart';

enum StatusLogin { noLogged, logged, loggining, none }

class UserProvider extends ChangeNotifier {
  User user;
  String token;
  String msg;

  StatusLogin status = StatusLogin.loggining;
  bool isLoggining = false;

  FocusNode emailFocusNode;
  FocusNode passFocusNode;
  FocusNode btnFocusNode;

  UserProvider() {
    this.user = User.empty();
    emailFocusNode = FocusNode();
    passFocusNode = FocusNode();
    btnFocusNode = FocusNode();
    this._init();
  }

  Future _init() async {
    final preferences = await SharedPreferences.getInstance();
    await Future.delayed(Duration(milliseconds: 200));
    this.token = preferences.getString("token");
    if (this.token != null) {
      final res = await UserController.refreshToken(this.token);
      if (res["ok"] == true) {
        this.status = StatusLogin.logged;
        this.user = User.fromMap(res["usuario"]) ?? null;
        this.token = res["token"];
      } else if (res["ok"] == false) {
        this.status = StatusLogin.noLogged;
        this.msg = res["msg"];
      } else {
        this.status = StatusLogin.none;
        this.msg = "Hubo un error con el servidor";
      }
    } else {
      this.status = StatusLogin.noLogged;
    }
    notifyListeners();
  }

  Future loggin() async {
    this.isLoggining = true;
    notifyListeners();
    final res = await UserController.logIn(this.user);
    if (res["ok"] == true) {
      this.user = User.fromMap(res["usuario"]) ?? null;
      this.token = res["token"];
      final preferences = await SharedPreferences.getInstance();
      preferences.setString("token", this.token);
      this.msg = res["msg"];
      this.isLoggining = false;
      notifyListeners();
      return true;
    } else if (res["ok"] == false) {
      this.isLoggining = false;
      notifyListeners();
      return res;
    }
  }

  Map<String, dynamic> toMap() {
    return {
      'user': user?.toMap(),
      'token': token,
    };
  }

  Future<bool> logOut() async {
    final logOut = await Preferences.deleteToken();
    if (logOut) {
      this.token = null;
      this.user = User.empty();
      this.msg = null;
      this.status = null;
    }
    return logOut;
  }
}
