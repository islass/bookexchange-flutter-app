import 'dart:async';

import 'package:animate_do/animate_do.dart';
import 'package:bookexchange/Helpers/Socket.dart';
import 'package:bookexchange/Models/Publicacion.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_marker_popup/extension_api.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong/latlong.dart';

class MapProvider extends ChangeNotifier {
  MapController mapController;
  PopupController popUpController;
  List<Marker> markers;

  Position currentPosition;
  Position lastPosition = Position(latitude: 19.860599, longitude: -98.9592);
  bool realTimeScan;
  StreamSubscription<Position> _scanRealTimeSuscription;

  MapProvider() {
    this.currentPosition = Position(latitude: 0, longitude: 0);
    this.mapController = MapController();
    this.popUpController = PopupController();
    this.markers = List<Marker>();
    this.realTimeScan = false;

    this._scanRealTimeSuscription = Geolocator.getPositionStream().listen(
      (position) async {
        this.currentPosition = position;
        MySocket.emitPosition(position);
        this.notifyListeners();
      },
    )..pause();
    // this._scanRealTimeSuscription.pause();
    this._getcurrentPosition();
  }

  void _getcurrentPosition() async {
    this.currentPosition = await Geolocator.getCurrentPosition();
    // this.lastPosition = this.currentPosition;
    notifyListeners();
    moveToCurrentPosition();
  }

  void moveToCurrentPosition() async {
    this.mapController.move(
          LatLng(this.currentPosition.latitude, this.currentPosition.longitude),
          16,
        );
  }

  void scanRealTime() {
    this.realTimeScan = !realTimeScan;
    (this.realTimeScan)
        ? this._scanRealTimeSuscription.resume()
        : this._scanRealTimeSuscription.pause();
    notifyListeners();
  }

  double _getKilometros() {
    final distancia = Geolocator.distanceBetween(
      this.currentPosition.latitude,
      this.currentPosition.longitude,
      this.lastPosition.latitude,
      this.lastPosition.longitude,
    );
    return distancia;
  }

  @override
  void dispose() {
    this._scanRealTimeSuscription.cancel();
    super.dispose();
  }

//
//
//Metodos no utlizados
  List<Marker> createMarkers(List<Publicacion> publicaciones) {
    this.markers = publicaciones.map((publicacion) {
      final point = LatLng(publicacion.posicion.coordenadas.latitude,
          publicacion.posicion.coordenadas.longitude);
      return Marker(
        point: point,
        builder: (context) => FadeInUp(
          key: ObjectKey(publicacion.idPublicacion),
          duration: Duration(milliseconds: 2000),
          delay: Duration(milliseconds: 1000),
          child: Icon(
            Icons.location_on,
            color: Colors.purple.withAlpha(200),
            // key: ObjectKey(publicacion.idPublicacion),
          ),
        ),
      );
    }).toList();
    return this.markers;
  }

  Stream<List<Marker>> getMarkersStream(
      List<Publicacion> publicaciones) async* {
    List<Marker> markers = List<Marker>();
    yield markers;
    await Future.delayed(Duration(milliseconds: 500));
    for (var publicacion in publicaciones) {
      await Future.delayed(Duration(milliseconds: 300));
      final point = LatLng(publicacion.posicion.coordenadas.latitude,
          publicacion.posicion.coordenadas.longitude);
      markers.add(
        Marker(
          point: point,
          builder: (context) => FadeInDown(
            key: ObjectKey(publicacion.idPublicacion),
            duration: Duration(milliseconds: 100),
            from: MediaQuery.of(context).size.height * .02,
            // delay: Duration(milliseconds: 1000),
            child: Icon(
              Icons.location_on,
              color: Colors.purple.withAlpha(200),
              // key: ObjectKey(publicacion.idPublicacion),
            ),
          ),
        ),
      );

      yield markers;
    }
  }
}
