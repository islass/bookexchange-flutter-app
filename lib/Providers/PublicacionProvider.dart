import 'package:flutter/material.dart';
import 'package:animate_do/animate_do.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:geohash/geohash.dart';

import 'package:bookexchange/Helpers/Socket.dart';
import 'package:bookexchange/Controllers/PublicacionesController.dart';
import 'package:bookexchange/Models/Publicacion.dart';

class PublicacionProvider extends ChangeNotifier {
  List<Publicacion> publicaciones;
  List<Marker> markers = List<Marker>();

  bool get isSocketConnected => MySocket.socket.connected;

  PublicacionProvider() {
    publicaciones = List<Publicacion>();
    _getPublicaciones();
  }

  void _getPublicaciones() async {
    final position = await Geolocator.getCurrentPosition();
    final geohash = Geohash.encode(position.latitude, position.longitude);
    final response = await PublicacionesController.getPublicaciones(
        geohash.substring(0, 4), position.latitude, position.longitude);
    if (response["ok"]) {
      this.publicaciones =
          (response["publicaciones"] as Iterable).map((publicacion) {
        return Publicacion.fromMap(publicacion);
      }).toList();
      notifyListeners();
    }
  }

  Stream<List<Marker>> getMarkersStream(BuildContext context) async* {
    //Eliminar marcadores sobrantes que no esten en la lista "publicaciones"
    if (this.markers.length > this.publicaciones.length) {
      for (var j = 0; j < this.markers.length; j++) {
        final key =
            ((this.markers[j].builder(context) as FadeInDown).key as ObjectKey)
                .value;
        final filter =
            this.publicaciones.where((element) => element.idPublicacion == key);
        if (filter.length == 0) {
          this.markers.removeAt(j);
        }
      }
    }

    ///
    yield this.markers;
    for (var publicacion in this.publicaciones) {
      final point = LatLng(publicacion.posicion.coordenadas.latitude,
          publicacion.posicion.coordenadas.longitude);

      final marker = this.markers.where((element) {
        final widget = (element.builder(context) as FadeInDown);
        return (widget.key as ObjectKey).value == publicacion.idPublicacion;
      });
      if (marker.length == 0) {
        await Future.delayed(Duration(milliseconds: 300));
        this.markers.add(
              Marker(
                point: point,
                builder: (context) => FadeInDown(
                  key: ObjectKey(publicacion.idPublicacion),
                  duration: Duration(milliseconds: 100),
                  from: MediaQuery.of(context).size.height * .02,
                  // delay: Duration(milliseconds: 1000),
                  child: Icon(
                    Icons.location_on,
                    color: Colors.purple.withAlpha(200),
                    // key: ObjectKey(publicacion.idPublicacion),
                  ),
                ),
              ),
            );
      }
      yield this.markers;
    }
  }

  void connectSocket() async {
    MySocket.connect();
    MySocket.onNewPublicacion((data) {
      this.publicaciones = (data as Iterable).map((e) {
        return Publicacion.fromMap(e);
      }).toList();
      notifyListeners();
    });
  }

  void disconnectSocket() async {
    MySocket.disconnect();
  }
}
