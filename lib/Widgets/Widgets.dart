import 'dart:ui';

import 'package:bookexchange/Models/Publicacion.dart';
import 'package:bookexchange/Pages/BookDetailsPage.dart';
import 'package:bookexchange/Pages/MyLibraryPage.dart';
import 'package:bookexchange/Providers/UserProvider.dart';
import 'package:bookexchange/Resources/Variables.dart';
import 'package:bookexchange/main.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CustomTextField extends StatelessWidget {
  final IconData icon;
  final String label;
  final Function(String) onChange;
  final TextInputType inputType;
  final bool obscureText;
  final double iconSize;
  final bool readOnly;
  final IconData suffixIcon;
  final Function() suffixOnPressed;
  final TextEditingController controller;
  final Function() onEditingComplete;
  final Function(String) onFieldSumitted;
  final FocusNode focusNode;
  final TextCapitalization textCapitalization;
  final String Function(String) validator;

  CustomTextField({
    @required this.label,
    @required this.icon,
    @required this.onChange,
    this.inputType = TextInputType.text,
    this.obscureText = false,
    this.iconSize = 24,
    this.readOnly = false,
    this.suffixIcon,
    this.suffixOnPressed,
    this.controller,
    this.onEditingComplete,
    this.onFieldSumitted,
    this.focusNode,
    this.textCapitalization,
    this.validator,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
        suffixIcon: this.suffixIcon != null
            ? IconButton(
                onPressed:
                    this.suffixIcon != null ? this.suffixOnPressed : null,
                icon: Icon(this.suffixIcon),
              )
            : null,
        labelText: this.label,
        icon: Icon(
          this.icon,
          size: this.iconSize,
        ),
      ),
      textCapitalization: this.textCapitalization ?? TextCapitalization.none,
      autocorrect: true,
      keyboardType: this.inputType,
      onChanged: this.onChange,
      focusNode: this.focusNode,
      obscureText: this.obscureText,
      controller: this.controller,
      readOnly: this.readOnly,
      onEditingComplete: this.onEditingComplete,
      onFieldSubmitted: this.onFieldSumitted,
      validator: this.validator,
    );
  }
}

class CustomRaisedButton extends StatelessWidget {
  final String texto;
  final Function() onClick;

  final FocusNode focusNode;

  const CustomRaisedButton({
    Key key,
    @required this.texto,
    @required this.onClick,
    this.focusNode,
  });

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: this.onClick,
      focusNode: this.focusNode,
      color: Colors.purple[200],
      child: Text(
        this.texto,
        style: TextStyle(fontSize: 20),
      ),
    );
  }
}

class CustomPopUpMarker extends StatelessWidget {
  final Publicacion _publicacion;

  const CustomPopUpMarker(this._publicacion);

  @override
  Widget build(BuildContext context) {
    final pantalla = MediaQuery.of(context).size;
    final popUpWidt = pantalla.width * .8;
    final popUpHeigth = pantalla.height * .2;
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      width: popUpHeigth * 1.98,
      height: popUpHeigth,
      alignment: Alignment.center,
      decoration: BoxDecoration(color: Colors.purple[100].withAlpha(220)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Hero(
            tag: "hero",
            child: CachedNetworkImage(
              width: popUpHeigth * .5,
              height: popUpHeigth * .75,
              fit: BoxFit.fill,
              imageUrl: _publicacion.libro.fotos.length > 0
                  ? _publicacion.libro.fotos[0]
                  : "https://image.flaticon.com/icons/png/512/16/16410.png",
              placeholder: (context, texto) => Container(
                child: CircularProgressIndicator(),
              ),
            ),
          ),
          VerticalDivider(),
          Expanded(
            child: Container(
              width: popUpWidt,
              height: popUpHeigth,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Text(
                    _publicacion.libro.titulo.toUpperCase(),
                    style: TextStyle(
                      fontSize: popUpHeigth * .15,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    _publicacion.libro.autor.toUpperCase(),
                    style: TextStyle(
                      fontSize: popUpHeigth * .08,
                    ),
                  ),
                  Divider(
                    color: Colors.black,
                    indent: popUpHeigth * .05,
                    height: popUpHeigth * .09,
                  ),
                  // Text("(${_publicacion.libro.idLibro})"),
                  Expanded(
                    child: Container(
                      alignment: Alignment.topLeft,
                      child: Text(
                        _publicacion.libro.descripcion.toUpperCase(),
                        // softWrap: true,
                        style: TextStyle(fontSize: popUpHeigth * .08),
                        overflow: TextOverflow.fade,
                      ),
                    ),
                  ),
                  // Text("Propietario: " + _publicacion.libro.propietario),
                  Container(
                    width: popUpHeigth * .9,
                    height: popUpHeigth * .25,
                    alignment: Alignment.centerRight,
                    child: FlatButton(
                      onPressed: () {
                        // _mapProvider.popUpController.hidePopup();
                        goTo(
                          context,
                          BookDetailsPage(
                            publicacion: _publicacion,
                          ),
                        );
                      },
                      child: Text(
                        "Ver  más...",
                        style: TextStyle(fontSize: popUpHeigth * .11),
                      ),
                    ),
                    // child: CustomRaisedButton(
                    //   onClick: () {},
                    //   texto: "Ver más...",
                    //   // goTo(context, BookDetailsPage());
                    // ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CustomDrawerMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final pantalla = MediaQuery.of(context).size;
    final menuWidth = pantalla.width * .7;
    final _userProvider = Provider.of<UserProvider>(context);
    return Container(
      padding: EdgeInsets.only(top: 40),
      color: Colors.purple[100],
      height: pantalla.height,
      width: menuWidth,
      child: Column(
        children: <Widget>[
          Stack(
            alignment: Alignment.bottomRight,
            children: <Widget>[
              CircleAvatar(
                radius: menuWidth * .25,
              ),
              IconButton(
                alignment: Alignment.topRight,
                tooltip: "Cambiar foto",
                icon: Icon(
                  Icons.add_a_photo,
                ),
                onPressed: () {},
              )
            ],
          ),
          Text(
            _userProvider.user.name,
            style: TextStyle(fontSize: menuWidth * .12),
          ),
          Divider(),
          Container(
            width: menuWidth,
            height: pantalla.height * .05,
            child: FlatButton(
              onPressed: () {},
              child: Text("Mi Perfil"),
            ),
          ),
          // Divider(),
          Container(
            width: menuWidth,
            height: pantalla.height * .05,
            child: FlatButton(
              onPressed: () {
                Navigator.pop(context);
                goTo(context, MyLibraryPage());
              },
              child: Text("Mi Biblioteca"),
            ),
          ),
          // Divider(),
          Container(
            width: menuWidth,
            height: pantalla.height * .05,
            child: FlatButton(
              onPressed: () {},
              child: Text("Configuraciones"),
            ),
          ),
          Container(
            width: menuWidth,
            height: pantalla.height * .05,
            child: FlatButton(
              onPressed: () async {
                final res = await _userProvider.logOut();
                if (res) replaceTo(context, MyApp());
              },
              child: Text("Cerrar sesión"),
            ),
          ),
        ],
      ),
    );
  }
}
