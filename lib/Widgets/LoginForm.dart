import 'package:bookexchange/Controllers/UserController.dart';
import 'package:bookexchange/Models/User.dart';
import 'package:bookexchange/Pages/MapPage.dart';
import 'package:bookexchange/Pages/RegisterPage.dart';
import 'package:bookexchange/Providers/UserProvider.dart';
import 'package:bookexchange/Resources/AlertDialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  @override
  Widget build(BuildContext context) {
    final _userProvider = Provider.of<UserProvider>(context);
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 30, bottom: 20),
              child: Icon(
                Icons.book,
                size: 150,
              ),
            ),
            // Text(_userProvider.email),
            TextFormField(
              decoration: InputDecoration(
                labelText: "Correo electronico",
                icon: Icon(Icons.alternate_email),
              ),
              keyboardType: TextInputType.emailAddress,
              onChanged: (correo) {
                _userProvider.user.email = correo;
              },
            ),
            TextFormField(
              decoration: InputDecoration(
                labelText: "Contraseña",
                icon: Icon(Icons.lock),
              ),
              obscureText: true,
              keyboardType: TextInputType.text,
              onChanged: (contrasenia) {
                _userProvider.user.password = contrasenia;
              },
            ),
            Container(
              margin: EdgeInsets.only(top: 20, bottom: 15),
              width: MediaQuery.of(context).size.width * .7,
              child: RaisedButton(
                onPressed: () async {
                  // final response =
                  // await UserController.logIn(_userProvider.user);
                  // if (response == true) {
                  //   Navigator.pushReplacement(
                  //       context, MaterialPageRoute(builder: (_) => MapPage()));
                  // } else {
                  //   buildShowDialog(context, "Error", response.toString());
                  // }
                },
                color: Colors.purple[200],
                child: Text(
                  "Iniciar Sesión",
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
            RaisedButton(
              onPressed: () {
                // UserController.getUser();
                // _userProvider.getUser();
                // _userProvider.cerrarSesion();
              },
              color: Colors.purple[200],
              child: Text(
                "Cerrar sesion",
                style: TextStyle(fontSize: 20),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Aún no tienes una cuenta?  "),
                InkWell(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => RegisterPage()));
                  },
                  child: Text(
                    "Registrate Aqui!!",
                    style: TextStyle(color: Colors.blue),
                  ),
                )
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              child: Column(
                children: <Widget>[
                  Divider(),
                  Text("Ó"),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(
                          FontAwesomeIcons.facebook,
                          color: Colors.blue,
                          size: 30,
                        ),
                        onPressed: () {},
                      ),
                      IconButton(
                        icon: Icon(
                          FontAwesomeIcons.googlePlus,
                          color: Colors.red,
                          size: 30,
                        ),
                        onPressed: () {},
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
