import 'package:animate_do/animate_do.dart';
import 'package:bookexchange/Pages/LoginPage.dart';
import 'package:bookexchange/Providers/PublicacionProvider.dart';
import 'package:bookexchange/Providers/UserProvider.dart';
import 'package:bookexchange/Widgets/Widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map_marker_popup/flutter_map_marker_popup.dart';
import 'package:provider/provider.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

import 'package:bookexchange/Pages/NewBookPage.dart';
import 'package:bookexchange/Providers/MapProvider.dart';
import 'package:bookexchange/Resources/Variables.dart';

class MapPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _publicacionProvider = Provider.of<PublicacionProvider>(context);
    // final pantalla = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        body: Builder(
          builder: (context) => Stack(
            children: <Widget>[
              StreamBuilder<List<Marker>>(
                stream: _publicacionProvider.getMarkersStream(context),
                builder: (context, snapshot) {
                  return Consumer<MapProvider>(
                    builder: (context, _mapProvider, child) => FlutterMap(
                      mapController: _mapProvider.mapController,
                      options: MapOptions(
                        plugins: [
                          PopupMarkerPlugin(),
                        ],
                        onTap: (position) {
                          _mapProvider.popUpController.hidePopup();
                        },
                        center: LatLng(
                          _mapProvider.currentPosition.latitude,
                          _mapProvider.currentPosition.longitude,
                        ),
                        zoom: 14.8,
                      ),
                      layers: [
                        TileLayerOptions(
                          urlTemplate:
                              "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                          subdomains: ['a', 'b', 'c'],
                        ),
                        CircleLayerOptions(
                          circles: [
                            CircleMarker(
                              point: LatLng(
                                _mapProvider.currentPosition.latitude,
                                _mapProvider.currentPosition.longitude,
                              ),
                              radius: 500,
                              color: Colors.cyan.withAlpha(100),
                              useRadiusInMeter: true,
                              borderColor: Colors.black,
                              borderStrokeWidth: 1,
                            ),
                          ],
                        ),
                        PopupMarkerLayerOptions(
                          popupController: _mapProvider.popUpController,
                          popupBuilder: (context, marker) {
                            final markerIcon =
                                (marker.builder(context) as FadeInDown);
                            final publicacion = _publicacionProvider
                                .publicaciones
                                .firstWhere((element) =>
                                    element.idPublicacion ==
                                    (markerIcon.key as ObjectKey).value);
                            return CustomPopUpMarker(publicacion);
                          },
                          markers: snapshot.data ?? List<Marker>(),
                        ),
                        MarkerLayerOptions(
                          markers: [
                            Marker(
                              point: LatLng(
                                _mapProvider.currentPosition.latitude,
                                _mapProvider.currentPosition.longitude,
                              ),
                              builder: (context) => Icon(
                                Icons.location_on,
                                color: Colors.black,
                                size: 28,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  );
                },
              ),
              Container(
                alignment: Alignment.topCenter,
                child: Consumer<MapProvider>(
                  builder: (context, value, child) => Column(
                    children: <Widget>[
                      Text(
                        value.currentPosition.toString(),
                        style: TextStyle(
                          fontSize: 16,
                          backgroundColor: Colors.grey[100],
                        ),
                      ),
                      (value.realTimeScan)
                          ? (_publicacionProvider.isSocketConnected == true)
                              ? Container(
                                  child: Icon(
                                    Icons.signal_wifi_4_bar,
                                    color: Colors.green,
                                  ),
                                )
                              : (_publicacionProvider.isSocketConnected ==
                                      false)
                                  ? Container(
                                      child: Icon(
                                        Icons.perm_scan_wifi,
                                        color: Colors.red,
                                      ),
                                    )
                                  : Container(
                                      child: CircularProgressIndicator())
                          : Container(),
                    ],
                  ),
                ),
              ),
              // Container(
              //   padding: EdgeInsets.only(right: 15),
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.end,
              //     children: [
              //       CustomContextMenu(),
              //     ],
              //   ),
              // ),
              Container(
                padding: EdgeInsets.only(top: 5, left: 5),
                alignment: Alignment.topLeft,
                child: IconButton(
                  icon: Icon(Icons.menu),
                  onPressed: () {
                    Scaffold.of(context).openDrawer();
                  },
                ),
              ),
              // Container(
              //   height: pantalla.height,
              //   child: IconButton(
              //     onPressed: () {},
              //     tooltip: "Menu",
              //     icon: Icon(
              //       Icons.arrow_right,
              //       color: Colors.blue,
              //       size: 40,
              //     ),
              //   ),
              // )
            ],
          ),
        ),
        persistentFooterButtons: <Widget>[
          Consumer<MapProvider>(
            builder: (context, mapProvider, child) => Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text("Escanear en tiempo real"),
                Switch(
                  onChanged: (value) {
                    mapProvider.scanRealTime();
                    if (value) {
                      _publicacionProvider.connectSocket();
                    } else {
                      _publicacionProvider.disconnectSocket();
                    }
                  },
                  value: mapProvider.realTimeScan,
                )
              ],
            ),
          )
        ],
        drawer: CustomDrawerMenu(),
        floatingActionButton: FloatingActionButton(
          tooltip: "Nuevo Libro",
          child: Icon(Icons.note_add),
          onPressed: () async {
            final res = await goTo(context, NewBookPage());
            if (res != null) {
              showDialog(
                context: context,
                child: AlertDialog(
                  content: Text(res["msg"]),
                ),
              );
            }
            // Scaffold.of(context).showSnackBar(SnackBar(
            //   content: Text("xzsdaf"),
            // ));
          },
        ),
      ),
    );
  }
}

class CustomContextMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _userProvider = Provider.of<UserProvider>(context, listen: false);
    return PopupMenuButton(
      onSelected: (boton) async {
        switch (boton) {
          case "Nuevo Libro":
            goTo(context, NewBookPage());
            break;
          case "Cerrar sesion":
            final logOut = await _userProvider.logOut();
            if (logOut) {
              replaceTo(context, LoginPage());
            } else {
              Scaffold.of(context).showSnackBar(SnackBar(
                content: Text("Intenta de nuevo"),
              ));
            }
            break;
          default:
        }
      },
      elevation: 10,
      tooltip: "Acciones",
      icon: Icon(
        Icons.more_horiz,
        size: 50,
        color: Colors.blue[700],
      ),
      // child: IconButton(
      //   icon: Icon(Icons.more_horiz),
      //   iconSize: 50,
      // ),
      color: Colors.cyan[200],
      itemBuilder: (context) {
        final botones = ["Nuevo Libro", "Acerca de...", "Cerrar sesion"];
        return botones.map(
          (e) {
            return PopupMenuItem(
              child: Text(e),
              value: e,
            );
          },
        ).toList();
      },
    );
  }
}
