import 'package:bookexchange/Pages/MapPage.dart';
import 'package:bookexchange/Providers/UserProvider.dart';
import 'package:bookexchange/Resources/Variables.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:bookexchange/Pages/RegisterPage.dart';
import 'package:bookexchange/Widgets/Widgets.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _userProvider = Provider.of<UserProvider>(context, listen: false);
    Size pantalla = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        body: Builder(builder: (context) {
          return SingleChildScrollView(
            child: Stack(
              // alignment: Alignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 30, bottom: 20),
                        child: Icon(
                          Icons.book,
                          size: 150,
                        ),
                      ),
                      // Text(_userProvider.email),
                      CustomTextField(
                        focusNode: _userProvider.emailFocusNode,
                        icon: Icons.email,
                        label: "Correo electronico",
                        onChange: (correo) {
                          _userProvider.user.email = correo;
                        },
                        onEditingComplete: () {
                          _userProvider.passFocusNode.requestFocus();
                        },
                        inputType: TextInputType.emailAddress,
                      ),
                      CustomTextField(
                        focusNode: _userProvider.passFocusNode,
                        icon: Icons.lock,
                        obscureText: true,
                        inputType: TextInputType.visiblePassword,
                        label: "Contraseña",
                        onChange: (contrasenia) {
                          _userProvider.user.password = contrasenia;
                        },
                        onEditingComplete: () async {
                          _userProvider.passFocusNode.unfocus();
                          final res = await _userProvider.loggin();
                          if (res is bool) {
                            replaceTo(context, MapPage());
                          } else if (res is Map) {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text(res.toString()),
                            ));
                          }
                          // replaceTo(context, MapPage());
                        },
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20, bottom: 15),
                        width: pantalla.width * .7,
                        child: CustomRaisedButton(
                          texto: "Iniciar sesión",
                          focusNode: _userProvider.btnFocusNode,
                          onClick: () async {
                            final res = await _userProvider.loggin();
                            if (res is bool) {
                              replaceTo(context, MapPage());
                            } else if (res is Map) {
                              Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text(res["msg"]),
                              ));
                            }
                            // replaceTo(context, MapPage());
                          },
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text("Aún no tienes una cuenta?  "),
                          InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => RegisterPage()));
                            },
                            child: Text(
                              "Registrate Aqui!!",
                              style: TextStyle(color: Colors.blue),
                            ),
                          )
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Column(
                          children: <Widget>[
                            Divider(),
                            Text("Ó"),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                IconButton(
                                  icon: Icon(
                                    FontAwesomeIcons.facebook,
                                    color: Colors.blue,
                                    size: 30,
                                  ),
                                  onPressed: () {},
                                ),
                                IconButton(
                                  icon: Icon(
                                    FontAwesomeIcons.googlePlus,
                                    color: Colors.red,
                                    size: 30,
                                  ),
                                  onPressed: () {},
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                (_userProvider.isLoggining)
                    ? Container(
                        width: pantalla.width,
                        height: pantalla.height,
                        alignment: Alignment.center,
                        color: Colors.black.withAlpha(70),
                        child: CircularProgressIndicator(),
                      )
                    : Container(),
              ],
            ),
          );
        }),
      ),
    );
  }
}
