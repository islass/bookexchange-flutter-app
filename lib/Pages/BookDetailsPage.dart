import 'package:bookexchange/Models/Publicacion.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class BookDetailsPage extends StatelessWidget {
  final Publicacion publicacion;

  const BookDetailsPage({Key key, @required this.publicacion})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    // final _user = Provider.of<User>(context, listen: false);
    final pantalla = MediaQuery.of(context).size;
    final cardWidth = pantalla.width;
    final cardHeigth = pantalla.height * .3;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Detalles"),
        ),
        body: SingleChildScrollView(
          child: Container(
            width: pantalla.width,
            height: pantalla.height,
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  width: cardWidth,
                  height: cardHeigth,
                  child: Card(
                    child: Row(
                      children: <Widget>[
                        Hero(
                          tag: "hero",
                          child: CachedNetworkImage(
                            width: cardWidth * .3,
                            height: cardHeigth * .9,
                            fit: BoxFit.fill,
                            imageUrl: this.publicacion.libro.fotos.length > 0
                                ? this.publicacion.libro.fotos[0]
                                : "https://image.flaticon.com/icons/png/512/16/16410.png",
                          ),
                        ),
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.only(top: cardHeigth * .05),
                            height: cardHeigth * .8,
                            alignment: Alignment.topCenter,
                            child: Text(
                                this.publicacion.libro.titulo.toUpperCase()),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: pantalla.width * .03),
                  width: cardWidth,
                  height: cardHeigth,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: cardWidth * .8,
                          height: cardHeigth,
                          child: Card(
                            child: Text("Tarjeta 1"),
                          ),
                        ),
                        Container(
                          width: cardWidth * .8,
                          height: cardHeigth,
                          child: Card(
                            child: Text("Tarjeta 2"),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
