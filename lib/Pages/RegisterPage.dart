import 'package:flutter/material.dart';
import 'package:bookexchange/Widgets/Widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class RegisterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size pantalla = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            width: pantalla.width,
            padding: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
            alignment: Alignment.center,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 30),
                  child: Icon(
                    FontAwesomeIcons.book,
                    size: 150,
                    color: Colors.cyan[300],
                  ),
                ),
                Text(
                  "Registrate",
                  style: TextStyle(fontSize: 45),
                ),
                CustomTextField(
                  icon: FontAwesomeIcons.userAlt,
                  iconSize: 20,
                  label: "Nombre Completo",
                  onChange: (nombre) {},
                ),
                CustomTextField(
                  icon: Icons.email,
                  label: "Correo electronico",
                  onChange: (correo) {},
                  inputType: TextInputType.emailAddress,
                ),
                CustomTextField(
                  icon: Icons.lock,
                  label: "Contraseña",
                  onChange: (correo) {},
                  inputType: TextInputType.visiblePassword,
                  obscureText: true,
                ),
                CustomTextField(
                  icon: Icons.lock,
                  label: "(Confirmar) Contraseña",
                  onChange: (correo) {},
                  inputType: TextInputType.visiblePassword,
                  obscureText: true,
                ),
                Container(
                  width: pantalla.width * .7,
                  margin: EdgeInsets.only(top: 20, bottom: 15),
                  child: CustomRaisedButton(
                    texto: "Registrarse",
                    onClick: () {
                      // Scaffold.of(context).showSnackBar(
                      //   SnackBar(
                      //     content: Text("Mensaje:"),
                      //   ),
                      // );
                      Navigator.pop(context);
                    },
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Ya tienes una cuenta??    "),
                    InkWell(
                      child: Text(
                        "Iniciar sesión!!",
                        style: TextStyle(color: Colors.blue),
                      ),
                      onTap: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
