import 'package:bookexchange/Widgets/LoginForm.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            LoginForm(),
          ],
        ),
      ),
    );
  }
}
