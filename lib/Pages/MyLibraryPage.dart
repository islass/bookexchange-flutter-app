import 'package:bookexchange/Providers/UserProvider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyLibraryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _libros = Provider.of<UserProvider>(context).user.libros;
    return SafeArea(
      child: Scaffold(
        body: Builder(
          builder: (context) => Container(
            // margin: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
            child: ListView.builder(
              padding: EdgeInsets.only(top: 15),
              itemCount: _libros.length,
              itemBuilder: (context, index) {
                final libro = _libros[index];
                return ExpansionTile(
                  title: ListTile(
                    contentPadding: EdgeInsets.symmetric(horizontal: 0),
                    leading: CircleAvatar(
                      radius: 25,
                      child: CachedNetworkImage(
                        imageUrl:
                            (libro.fotos.length > 0) ? libro.fotos[0] : "",
                      ),
                    ),
                    title: Text("Titulo: ${libro.titulo.toUpperCase()}"),
                    subtitle: Text("Autor: ${libro.autor.toUpperCase()}"),
                  ),
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 20, bottom: 20),
                      child: Text(
                        "Detalles",
                        style: TextStyle(fontSize: 26),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 25, bottom: 20),
                      width: double.infinity,
                      alignment: Alignment.topLeft,
                      child: Column(
                        // mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                              "Descripción: ${libro.descripcion.toUpperCase()}"),
                          Text("No.Paginas: ${libro.noPaginas}"),
                          Text("Propietario: ${libro.propietario}"),
                        ],
                      ),
                    )
                  ],
                  // subtitle: Text("Expandido"),
                  // trailing: Text("Expandido"),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
