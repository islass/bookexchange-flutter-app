import 'package:bookexchange/Providers/MapProvider.dart';
import 'package:bookexchange/Providers/UserProvider.dart';
import 'package:flutter/material.dart';
import 'package:bookexchange/Providers/LibroProvider.dart';
import 'package:bookexchange/Widgets/Widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class NewBookPage extends StatefulWidget {
  @override
  _NewBookPageState createState() => _NewBookPageState();
}

class _NewBookPageState extends State<NewBookPage> {
  @override
  Widget build(BuildContext context) {
    final pantalla = MediaQuery.of(context).size;
    final _libroProvider = Provider.of<LibroProvider>(context);
    final _userProvider = Provider.of<UserProvider>(context, listen: false);
    final _mapProvider = Provider.of<MapProvider>(context, listen: false);
    
    return SafeArea(
      child: Scaffold(
        body: Builder(
          builder: (context) => Stack(
            children: <Widget>[
              SingleChildScrollView(
                child: Container(
                  width: pantalla.width,
                  height: pantalla.height,
                  margin: EdgeInsets.only(left: 25, right: 25, top: 30),
                  alignment: Alignment.center,
                  child: Form(
                    key: _libroProvider.formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Agregar nuevo libro",
                          style: TextStyle(fontSize: pantalla.width * .1),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 15),
                          width: pantalla.width,
                          height: pantalla.height * .52,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              // border: Border.all(color: Colors.black, width: .5),
                              ),
                          child: Stack(
                            alignment: Alignment.bottomRight,
                            children: <Widget>[
                              SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: (_libroProvider.fotos.length > 0)
                                      ? <Widget>[
                                          Row(
                                            mainAxisSize: MainAxisSize.max,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: _libroProvider
                                                .getFotos(context),
                                          )
                                        ]
                                      : <Widget>[
                                          Container(
                                            width: pantalla.width,
                                            alignment: Alignment.center,
                                            child: Text(
                                              "Fotos",
                                              style: TextStyle(
                                                color:
                                                    Colors.grey.withAlpha(100),
                                                fontSize: pantalla.width * .2,
                                              ),
                                            ),
                                          ),
                                        ],
                                ),
                              ),
                              IconButton(
                                tooltip: "Agregar fotos(s)",
                                onPressed: () async {
                                  _libroProvider.tituloFocusNode.unfocus();
                                  _libroProvider.autorFocusNode.unfocus();
                                  _libroProvider.descripcionFocusNode.unfocus();
                                  _libroProvider.noPaginasFocusNode.unfocus();
                                  await _libroProvider.addFotos(context);
                                },
                                icon: CircleAvatar(
                                  foregroundColor: Colors.white,
                                  backgroundColor: Colors.black,
                                  child: Icon(Icons.add_a_photo),
                                ),
                                color: Colors.white,
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          child: CustomTextField(
                            controller: _libroProvider.barCodeController,
                            icon: FontAwesomeIcons.barcode,
                            suffixOnPressed: () async {
                              final res = await _libroProvider
                                  .scanBarCode(_userProvider.user.idUsuario);
                              if (res != null) {
                                final dialog = await showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  child: AlertDialog(
                                    content: Text(res["msg"]),
                                    title: Text("Error"),
                                    actions: <Widget>[
                                      FlatButton(
                                        child: Text("Volver"),
                                        onPressed: () {
                                          Navigator.pop(context, true);
                                        },
                                      ),
                                      FlatButton(
                                        child: Text("Aceptar"),
                                        onPressed: () {
                                          Navigator.pop(context, false);
                                        },
                                      ),
                                    ],
                                  ),
                                );
                                if (dialog) {
                                  Navigator.pop(context);
                                } else {
                                  _libroProvider.limpiarCampos();
                                }
                              } else {
                                if (_libroProvider.libro.titulo.isEmpty ||
                                    _libroProvider.libro.titulo == null) {
                                  _libroProvider.tituloFocusNode.requestFocus();
                                }
                              }
                            },
                            iconSize: 20,
                            suffixIcon: Icons.camera_alt,
                            readOnly: true,
                            label: 'Código de barras ("Escanear...")',
                            onChange: (codigoBarras) {},
                            // onEditingComplete: (){
                            //   _libroProvider.autorFocusNode.requestFocus();
                            // },
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: CustomTextField(
                            focusNode: _libroProvider.tituloFocusNode,
                            controller: _libroProvider.tituloController,
                            textCapitalization: TextCapitalization.characters,
                            icon: FontAwesomeIcons.bookOpen,
                            label: "Título",
                            iconSize: 20,
                            onChange: (titulo) {
                              _libroProvider.titulo = titulo;
                            },
                            onEditingComplete: () {
                              _libroProvider.titulo =
                                  _libroProvider.tituloController.text;
                              _libroProvider.autorFocusNode.requestFocus();
                            },
                            validator: (titulo) {
                              if (titulo.isEmpty) {
                                return "El Campo es requerido";
                              }
                            },
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: CustomTextField(
                            focusNode: _libroProvider.autorFocusNode,
                            controller: _libroProvider.autorController,
                            textCapitalization: TextCapitalization.characters,
                            icon: FontAwesomeIcons.userEdit,
                            label: "Autor",
                            iconSize: 20,
                            onChange: (autor) {
                              _libroProvider.autor = autor;
                            },
                            onEditingComplete: () {
                              _libroProvider.autor =
                                  _libroProvider.autorController.text;
                              _libroProvider.descripcionFocusNode
                                  .requestFocus();
                            },
                            validator: (autor) {
                              if (autor.isEmpty) {
                                return "El Campo es requerido";
                              }
                            },
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: CustomTextField(
                            focusNode: _libroProvider.descripcionFocusNode,
                            controller: _libroProvider.descripcionController,
                            textCapitalization: TextCapitalization.characters,
                            icon: FontAwesomeIcons.paragraph,
                            label: "Descripción",
                            iconSize: 20,
                            onChange: (descripcion) {
                              _libroProvider.descripcion = descripcion;
                            },
                            onEditingComplete: () {
                              _libroProvider.descripcion =
                                  _libroProvider.descripcionController.text;
                              _libroProvider.noPaginasFocusNode.requestFocus();
                              if (_libroProvider.noPaginasController.text ==
                                  "0") {
                                _libroProvider.noPaginasController.text = "";
                              }
                            },
                            validator: (descripcion) {
                              if (descripcion.isEmpty) {
                                return "El Campo es requerido";
                              }
                            },
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 20),
                          child: CustomTextField(
                            focusNode: _libroProvider.noPaginasFocusNode,
                            controller: _libroProvider.noPaginasController,
                            icon: FontAwesomeIcons.sortNumericUp,
                            label: "No.Páginas",
                            iconSize: 20,
                            onChange: (paginas) {
                              if (paginas.isEmpty) {
                                _libroProvider.noPaginas = 0;
                              }
                              _libroProvider.noPaginas =
                                  int.parse(paginas) ?? 0;
                            },
                            validator: (noPaginas) {
                              if (noPaginas.isEmpty) {
                                return "El Campo es requerido";
                              }
                            },
                            inputType: TextInputType.number,
                          ),
                        ),
                        // Tags(),
                      ],
                    ),
                  ),
                ),
              ),
              (_libroProvider.status == Status.registrando)
                  ? Container(
                      width: pantalla.width,
                      height: pantalla.height,
                      alignment: Alignment.center,
                      color: Colors.black.withAlpha(70),
                      child: CircularProgressIndicator(),
                    )
                  : Container(),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add_box),
          tooltip: "Añadir libro",
          backgroundColor: (_libroProvider.check) ? Colors.green : Colors.grey,
          onPressed: (_libroProvider.check)
              ? () async {
                  FocusNode().unfocus();
                  final res = await _libroProvider.crearLibro(_mapProvider.currentPosition);
                  // Navigator.pop(context, res);
                }
              : null,
        ),
      ),
    );
  }
}

// class Tags extends StatelessWidget {
//   const Tags({
//     Key key, Axis direction, TextDirection textDirection, VerticalDirection verticalDirection, int itemCount, int columns, TagsTextField textField, ItemTags Function(index)index, ItemTags Function(index)index)index) itemBuilder)index) itemBuilder,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Tags(
//       // alignment: WrapAlignment.start,
//       direction: Axis.horizontal,
//       // runAlignment: WrapAlignment.start,
//       textDirection: TextDirection.rtl,
//       verticalDirection: VerticalDirection.up,
//       itemCount: 3,
//       columns: 4,
//       textField: TagsTextField(
//         autofocus: false,
//         width: double.infinity,
//         hintText: "Tags",
//         inputDecoration: InputDecoration(
//           icon: Icon(
//             FontAwesomeIcons.tags,
//             size: 20,
//           ),
//         ),
//       ),
//       itemBuilder: (index) {
//         return ItemTags(
//           title: "Tags",
//           index: 0,
//         );
//       },
//     );
//   }
// }
