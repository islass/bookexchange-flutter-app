import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  static SharedPreferences preferences;

  Preferences() {
    _init();
  }

  void _init() async {
    preferences = await SharedPreferences.getInstance();
  }

  static Future<String> getToken() async {
    if (preferences != null) {
      return preferences.getString("token");
    } else {
      preferences = await SharedPreferences.getInstance();
      return preferences.getString("token");
    }
  }

  static Future<bool> deleteToken() async {
    if (preferences != null) {
      return await preferences.remove("token");
    } else {
      preferences = await SharedPreferences.getInstance();
      return await preferences.remove("token");
    }
  }
}
