import 'package:geohash/geohash.dart';
import 'package:geolocator/geolocator.dart';
import 'package:socket_io_client/socket_io_client.dart';

class MySocket {
  static Socket socket;

  static void connect() {
    socket = io('http://192.168.8.111:3000', <String, dynamic>{
      'transports': ['websocket'],
      'autoConnect': false,
      'extraHeaders': {'id': '5fa76217ea8b4e3484312ef0'}
    });
    socket.connect();
    socket.on("connected", (data) => print(data));
  }

  static void disconnect() {
    socket.disconnect()..close();
  }

  static void onNewPublicacion(dynamic Function(dynamic data) callBack) {
    socket.on("getPublicaciones", callBack);
  }

  static emitPosition(Position position) {
    socket.emit("newPosition", {
      'geohash':
          Geohash.encode(position.latitude, position.longitude).substring(0, 4),
      "position": {
        "lat": position.latitude,
        "long": position.longitude,
      },
    });
  }
}
