import 'package:bookexchange/Pages/LoginPage.dart';
import 'package:bookexchange/Providers/LibroProvider.dart';
import 'package:bookexchange/Providers/MapProvider.dart';
import 'package:bookexchange/Providers/PublicacionProvider.dart';
import 'package:bookexchange/Providers/UserProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:bookexchange/Pages/MapPage.dart';
import 'package:bookexchange/Resources/CircularIndicator.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<UserProvider>(
            create: (context) => UserProvider()),
        ChangeNotifierProvider<MapProvider>(create: (context) => MapProvider()),
        ChangeNotifierProvider<PublicacionProvider>(
          create: (context) => PublicacionProvider(),
        ),
        ChangeNotifierProvider<LibroProvider>(
            create: (context) => LibroProvider()),
      ],
      builder: (context, child) {
        final _userProvider = Provider.of<UserProvider>(context);
        return MaterialApp(
          title: 'Book Exchange',
          home: (_userProvider.status == StatusLogin.loggining)
              ? CircularIndicator()
              : (_userProvider.status == StatusLogin.logged)
                  ? MapPage()
                  : LoginPage(),
          debugShowCheckedModeBanner: false,
        );
      },
    );
  }
}
